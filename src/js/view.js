var Functions = require('./api.js')

window.addEventListener('scroll', function() {
  Functions.animateOnScroll();
});

(function() {
   Functions.animateOnLoad();

})();
