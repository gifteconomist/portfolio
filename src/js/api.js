var Functions = (function(){
  function animateOnScroll() {
    var el = document.body.querySelectorAll('[data-scroll="animate"]');
    el.forEach(function(element, i) {
      var _el = el[i];
      if (Functions.isInViewport(_el, 300)) {
        el[i].classList.add("animate");
      };
    });
  }

  function isInViewport(el, anticpateAmount) {
    var rect = el.getBoundingClientRect();
    var html = document.documentElement;
    return (
      rect.top + anticpateAmount >= 0 &&
      rect.left >= 0 &&
      rect.bottom - anticpateAmount <= (window.innerHeight || html.clientHeight) &&
      rect.right <= (window.innerWidth || html.clientWidth)
    );
  }

  function animateOnLoad() {
    var el = document.body.querySelectorAll('[data-onload="animate"]');
    el.forEach(function(element, i) {
      el[i].classList.add("animate");
    });
  }

  return {
		isInViewport: isInViewport,
    animateOnScroll: animateOnScroll,
    animateOnLoad: animateOnLoad
	};

})();

module.exports =  Functions;
